"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SharedModule = exports.initApp = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var http_1 = require("@angular/common/http");
var core_2 = require("@ngx-translate/core");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
function initApp(http, translate) {
    return function () { return new Promise(function (resolve) {
        var defaultLocale = 'en';
        var storageLocale = localStorage.getItem('locale');
        var locale = storageLocale || defaultLocale;
        var filesByLanguage = 12;
        var acceptLanguage = true;
        function getfileLocation_i18n(component) {
            return http.get("../../../assets/i18n/" + locale + "_" + component + ".json");
        }
        function errorHandler(error) {
            return operators_1.catchError(function () { return rxjs_1.of(console.error(error)); });
        }
        rxjs_1.forkJoin([
            getfileLocation_i18n('home').pipe(errorHandler('error en home')),
            getfileLocation_i18n('test1').pipe(errorHandler('error en test1')),
            getfileLocation_i18n('test2').pipe(errorHandler('error en test2')),
            getfileLocation_i18n('translate').pipe(errorHandler('error en translate')),
            getfileLocation_i18n('top').pipe(errorHandler('error en top'))
        ]).subscribe(function (response) {
            for (var i = 0; i < filesByLanguage; i++) {
                translate.setTranslation(locale, response[i], acceptLanguage);
            }
            translate.setDefaultLang(defaultLocale);
            translate.use(locale);
            resolve(true);
        });
    }); };
}
exports.initApp = initApp;
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        core_1.NgModule({
            declarations: [],
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                http_1.HttpClientModule,
                core_2.TranslateModule.forRoot()
            ],
            providers: [{
                    provide: core_1.APP_INITIALIZER,
                    useFactory: initApp,
                    deps: [http_1.HttpClient, core_2.TranslateService],
                    multi: true
                }],
            exports: [
                common_1.CommonModule,
                router_1.RouterModule,
            ]
        })
    ], SharedModule);
    return SharedModule;
}());
exports.SharedModule = SharedModule;
