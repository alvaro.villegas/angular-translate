import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { of, forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';

export function initApp(http: HttpClient, translate: TranslateService) {
  return () => new Promise<boolean>((resolve: (res: boolean) => void) => {
    const defaultLocale = 'en';
    const storageLocale = localStorage.getItem('locale');
    const locale = storageLocale || defaultLocale;
    const filesByLanguage = 12;
    const acceptLanguage = true;

    function getfileLocation_i18n(component: string) {
      return http.get(`../../../assets/i18n/${locale}_${component}.json`);
    }

    function errorHandler(error: string) {
      return catchError(() => of(console.error(error)));
    }

    forkJoin([
      getfileLocation_i18n('home').pipe(errorHandler('error en home')),
      getfileLocation_i18n('test1').pipe(errorHandler('error en test1')),
      getfileLocation_i18n('test2').pipe(errorHandler('error en test2')),
      getfileLocation_i18n('translate').pipe(errorHandler('error en translate')),
      getfileLocation_i18n('top').pipe(errorHandler('error en top'))
    ]).subscribe((response: any[]) => {
      for (let i = 0; i < filesByLanguage; i++) {
        translate.setTranslation(locale, response[i], acceptLanguage);
      }
      translate.setDefaultLang(defaultLocale);
      translate.use(locale);
      resolve(true);
    });
  });
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    TranslateModule.forRoot()
  ],
  providers: [{
    provide: APP_INITIALIZER,
    useFactory: initApp,
    deps: [HttpClient, TranslateService],
    multi: true,
  }],
  exports: [
    CommonModule,
    RouterModule,
  ],
})
export class SharedModule {
}

