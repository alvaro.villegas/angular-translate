"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BaseModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var top_component_1 = require("../base/top/top.component");
var content_component_1 = require("../base/content/content.component");
var translate_component_1 = require("./translate/translate.component");
var core_2 = require("@ngx-translate/core");
var shared_module_1 = require("./shared/shared.module");
var BaseModule = /** @class */ (function () {
    function BaseModule() {
    }
    BaseModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, core_2.TranslateModule, common_1.CommonModule, shared_module_1.SharedModule],
            exports: [top_component_1.TopComponent, content_component_1.ContentComponent, translate_component_1.TranslateComponent],
            providers: [],
            declarations: [top_component_1.TopComponent, content_component_1.ContentComponent, translate_component_1.TranslateComponent]
        })
    ], BaseModule);
    return BaseModule;
}());
exports.BaseModule = BaseModule;
