import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopComponent } from '../base/top/top.component';
import { ContentComponent } from '../base/content/content.component';
import { TranslateComponent } from './translate/translate.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from './shared/shared.module';
@NgModule({
  imports: [CommonModule, TranslateModule, CommonModule, SharedModule],
  exports: [TopComponent, ContentComponent, TranslateComponent],
  providers: [],
  declarations: [TopComponent, ContentComponent, TranslateComponent]
})
export class BaseModule { }
