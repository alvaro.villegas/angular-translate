import { Test2Component } from './test2.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { Test2RoutingModule } from './test2-routing.module';

@NgModule({
  declarations: [Test2Component],
  imports: [CommonModule, TranslateModule, Test2RoutingModule],
})
export class Test2Module {}
