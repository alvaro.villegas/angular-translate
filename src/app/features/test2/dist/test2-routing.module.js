"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.Test2RoutingModule = void 0;
var test2_component_1 = require("./test2.component");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var routes = [{ path: '', component: test2_component_1.Test2Component }];
var Test2RoutingModule = /** @class */ (function () {
    function Test2RoutingModule() {
    }
    Test2RoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], Test2RoutingModule);
    return Test2RoutingModule;
}());
exports.Test2RoutingModule = Test2RoutingModule;
