"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.Test1Module = void 0;
var test1_component_1 = require("./test1.component");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var core_2 = require("@ngx-translate/core");
var test1_routing_module_1 = require("./test1-routing.module");
var Test1Module = /** @class */ (function () {
    function Test1Module() {
    }
    Test1Module = __decorate([
        core_1.NgModule({
            declarations: [test1_component_1.Test1Component],
            imports: [common_1.CommonModule, core_2.TranslateModule, test1_routing_module_1.Test1RoutingModule]
        })
    ], Test1Module);
    return Test1Module;
}());
exports.Test1Module = Test1Module;
