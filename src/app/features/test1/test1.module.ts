import { Test1Component } from './test1.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { Test1RoutingModule } from './test1-routing.module';
@NgModule({
  declarations: [Test1Component],
  imports: [CommonModule, TranslateModule, Test1RoutingModule],
})
export class Test1Module {}
