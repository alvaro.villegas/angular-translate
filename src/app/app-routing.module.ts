import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./features/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'test1',
    loadChildren: () =>
      import('./features/test1/test1.module').then((m) => m.Test1Module),
  },
  {
    path: 'test2',
    loadChildren: () =>
      import('./features/test2/test2.module').then((m) => m.Test2Module),
  },
  {
    path: '', redirectTo: 'home', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
